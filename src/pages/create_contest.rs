use std::collections::HashMap;
use std::fs;
use std::fs::{create_dir_all, File};
use std::io::{Cursor, Read, Write};
use std::iter::FromIterator;
use std::path::PathBuf;

use actix_multipart::Multipart;
use chrono::{Local, NaiveDateTime};
use futures::{StreamExt, TryStreamExt};
use lazy_static::lazy_static;
use log::info;
use regex::Regex;
use tokio::sync::broadcast;

use crate::models::{contest, problem};
use crate::pages::prelude::*;
use crate::queue::job_protocol::{job_result, Job, JobResult};
use crate::{import_contest, language};

#[post("/contests/")]
async fn create_contest(
    identity: Identity,
    pool: Data<DbPool>,
    mut payload: Multipart,
    job_sender: Data<Sender<Job>>,
    job_result_sender: Data<broadcast::Sender<JobResult>>,
    tz: Data<Tz>,
    request: HttpRequest,
) -> PageResult {
    let logged_user = require_identity(&identity)?;
    if !logged_user.is_admin {
        return Err(PageError::Forbidden(
            "Apenas administradores podem fazer isso".into(),
        ));
    }

    #[derive(Debug)]
    struct Form {
        name: Option<String>,
        start_instant: Option<String>,
        end_instant: Option<String>,
        polygon_zip: Option<Cursor<Vec<u8>>>,
        grade_ratio: Option<i32>,
        grade_after_ratio: Option<i32>,
    }

    let mut form = Form {
        name: None,
        start_instant: None,
        end_instant: None,
        polygon_zip: None,
        grade_ratio: None,
        grade_after_ratio: None,
    };

    while let Ok(Some(mut field)) = payload.try_next().await {
        let mut cursor = Cursor::new(vec![]);
        while let Some(chunk) = field.next().await {
            let data = chunk.unwrap();
            cursor
                .write(&data)
                .map_err(|_| PageError::Validation("Corpo inválido".into()))?;
        }

        cursor.set_position(0);

        fn parse_field(field: &str, cursor: &mut Cursor<Vec<u8>>) -> Result<String, PageError> {
            let mut value = String::new();
            cursor
                .read_to_string(&mut value)
                .map_err(|_| PageError::Validation(format!("Campo {} inválido", field)))?;
            Ok(value)
        }

        match field.content_disposition().get_name() {
            Some("name") => form.name = Some(parse_field("name", &mut cursor)?),
            Some("start_instant") => {
                form.start_instant = Some(parse_field("start_instant", &mut cursor)?)
            }
            Some("end_instant") => {
                form.end_instant = Some(parse_field("end_instant", &mut cursor)?)
            }
            Some("grade_ratio") => {
                form.grade_ratio = parse_field("grade_ratio", &mut cursor)?.parse().ok()
            }
            Some("grade_after_ratio") => {
                form.grade_after_ratio = parse_field("grade_after_ratio", &mut cursor)?.parse().ok()
            }
            Some("polygon_zip") => form.polygon_zip = Some(cursor),
            _ => {}
        }
    }

    let polygon_zip = form
        .polygon_zip
        .ok_or_else(|| PageError::Validation("Arquivo não informado".into()))?;
    let imported = import_contest::import_file(polygon_zip)
        .map_err(|e| PageError::Validation(format!("Não foi possível importar: {}", e)))?;
    let mut connection = pool.get()?;

    let contest = if form.name.as_ref().unwrap() != "" {
        Some(contest::insert_contest(
            &mut connection,
            contest::NewContest {
                name: form.name.clone().unwrap(),
                start_instant: form
                    .start_instant
                    .and_then(|s| NaiveDateTime::parse_from_str(&s, "%Y-%m-%d %H:%M:%S").ok())
                    .and_then(|d| d.and_local_timezone(**tz).single())
                    .map(|d| d.naive_utc()),
                end_instant: form
                    .end_instant
                    .and_then(|s| NaiveDateTime::parse_from_str(&s, "%Y-%m-%d %H:%M:%S").ok())
                    .and_then(|d| d.and_local_timezone(**tz).single())
                    .map(|d| d.naive_utc()),
                creation_instant: Local::now().naive_utc(),
                creation_user_id: logged_user.id,
                grade_ratio: form.grade_ratio,
                grade_after_ratio: form.grade_after_ratio,
            },
        )?)
    } else {
        None
    };

    fn polygon_url_to_id_without_revision(url: String) -> String {
        url.replace("https://polygon.codeforces.com/", "polygon.")
            .replace('/', ".")
    }

    let problem_label: HashMap<String, String> =
        HashMap::from_iter(imported.0.problems.problem.iter().map(|problem| {
            (
                polygon_url_to_id_without_revision(problem.url.clone()),
                problem.index.clone(),
            )
        }));

    let mut zip = imported.2;

    lazy_static! {
        static ref CODEFORCES_LANGUAGE_TO_JUGHISTO: HashMap<String, String> = {
            let mut m = HashMap::new();
            m.insert("cpp.g++17".into(), "cpp.17.g++".into());
            m.insert("cpp.g++20".into(), "cpp.20.g++".into());
            m.insert("cpp.msys2-mingw64-9-g++17".into(), "cpp.17.g++".into());
            m.insert("cpp.msys2-mingw64-9-g++20".into(), "cpp.20.g++".into());
            m.insert("java.8".into(), "java.8".into());
            m.insert("testlib".into(), "cpp.20.g++".into());
            m
        };
    }

    for (name, metadata) in imported.1 {
        let problem_id_without_revision = polygon_url_to_id_without_revision(metadata.url);
        let problem_id = format!("{}.r{}", problem_id_without_revision, &metadata.revision);

        let files_regex: Regex = Regex::new(&format!(
            concat!(
                "^{}/(",
                r"files/$|",
                r"files/.*\.cpp$|",
                r"files/.*\.h$|",
                r"files/tests/$|",
                r"files/tests/validator-tests/$|",
                r"files/tests/validator-tests/.*$|",
                r"files/tests/validator-tests/.*$|",
                r"solutions/$|",
                r"solutions/.*.cc$|",
                r"solutions/.*.cpp$|",
                r"statements/$|",
                r"statements/.html/.*$|",
                r"tests/$",
                ")"
            ),
            name
        ))
        .unwrap();
        let mut filenames = zip
            .file_names()
            .filter(|name| files_regex.is_match(name))
            .map(std::string::ToString::to_string)
            .collect::<Vec<_>>();
        filenames.sort();
        for name in filenames {
            let relative_path = files_regex
                .captures(&name)
                .unwrap()
                .get(1)
                .unwrap()
                .as_str();
            let data_path = format!("/data/{}/{}", problem_id, relative_path);

            if name.ends_with('/') {
                info!("Creating directory {} into {}", name, data_path);
                create_dir_all(data_path)?;
                continue;
            }

            info!("Putting file {} into {}", name, data_path);
            std::io::copy(&mut zip.by_name(&name)?, &mut File::create(data_path)?)?;
        }

        fn map_codeforces_language(input: &String) -> Result<String, PageError> {
            Ok(CODEFORCES_LANGUAGE_TO_JUGHISTO
                .get(input)
                .ok_or_else(|| PageError::Validation(format!("Linguagem {} não suportada", input)))?
                .into())
        }

        let main_solution = &metadata
            .assets
            .solutions
            .solution
            .iter()
            .find(|s| s.tag == "main")
            .ok_or_else(|| PageError::Validation("No main solution".into()))?
            .source;

        let problem = problem::upsert_problem(
            &mut connection,
            problem::NewProblem {
                id: problem_id.clone(),
                name: metadata.names.name[0].value.clone(),
                memory_limit_bytes: metadata.judging.testset[0]
                    .memory_limit
                    .parse()
                    .unwrap(),
                time_limit_ms: metadata.judging.testset[0]
                    .time_limit
                    .parse()
                    .unwrap(),
                checker_path: metadata.assets.checker.source.path.clone(),
                checker_language: map_codeforces_language(&metadata.assets.checker.r#type)?,
                validator_path: metadata.assets.validators.validator[0].source.path.clone(),
                validator_language: map_codeforces_language(
                    &metadata.assets.validators.validator[0].source.r#type,
                )?,
                main_solution_path: main_solution.path.clone(),
                main_solution_language: map_codeforces_language(&main_solution.r#type)?,
                test_pattern: metadata.judging.testset[0].input_path_pattern.clone(),
                test_count: metadata.judging.testset[0]
                    .test_count
                    .parse()
                    .unwrap(),
                status: "compiled".into(),
                creation_instant: Local::now().naive_utc(),
                creation_user_id: logged_user.id,
            },
        )?;

        for (i, test) in metadata.judging.testset[0].tests.test.iter().enumerate() {
            let i = i + 1;
            let test_path = format!(
                "./{}/{}",
                problem_id,
                import_contest::format_width(&problem.test_pattern, i)
            );

            info!(
                "Iterating through test {} to {:#?}, which is {}",
                i,
                test_path,
                test.method.as_ref().unwrap()
            );
            if test.method.as_ref().unwrap() == "manual" {
                let test_name = PathBuf::from(&name)
                    .join(import_contest::format_width(&problem.test_pattern, i));
                info!("Extracting {:#?} from zip", test_name);
                std::io::copy(
                    &mut zip.by_name(test_name.to_str().unwrap())?,
                    &mut File::create(PathBuf::from("/data/").join(&test_path))?,
                )?;
            } else {
                let cmd: Vec<_> = test.cmd.as_ref().unwrap().split(' ').collect();
                let run_stats = language::run_cached(
                    &job_sender,
                    &job_result_sender,
                    &"cpp.20.g++".into(),
                    format!("./{}/files/{}.cpp", problem.id, cmd.first().unwrap()),
                    cmd[1..].iter().map(|s| <&str>::clone(s).into()).collect(),
                    None,
                    Some(test_path.clone()),
                    problem.memory_limit_bytes / 1_024,
                    10_000,
                )
                .await
                .map_err(|_| {
                    PageError::Validation("Couldn't use an intermediate program".into())
                })?;

                if run_stats.result != i32::from(job_result::run_cached::Result::Ok) {
                    return Err(PageError::Validation(
                        "Couldn't run an intermediate program".into(),
                    ));
                }
            }

            let run_stats = language::run_cached(
                &job_sender,
                &job_result_sender,
                &problem.main_solution_language,
                format!("./{}/{}", problem.id, problem.main_solution_path),
                vec![],
                Some(test_path.clone()),
                Some(format!("{}.a", test_path)),
                problem.memory_limit_bytes / 1_024,
                problem.time_limit_ms,
            )
            .await
            .map_err(|_| PageError::Validation("Couldn't run solution on test".into()))?;
            if run_stats.exit_code != 0 {
                return Err(PageError::Validation(
                    "Couldn't run solution on test".into(),
                ));
            }
        }

        language::judge(
            &job_sender,
            &job_result_sender,
            &problem.main_solution_language,
            fs::read_to_string(PathBuf::from(format!(
                "/data/{}/{}",
                problem.id, problem.main_solution_path
            )))?,
            problem.test_count,
            format!("./{}/{}", problem.id, problem.test_pattern),
            problem.checker_language,
            format!("./{}/{}", problem.id, problem.checker_path),
            problem.memory_limit_bytes / 1_024,
            problem.time_limit_ms,
        )
        .await
        .map_err(|_| PageError::Validation("Couldn't judge main solution".into()))?;

        if form.name.as_ref().unwrap() != "" {
            contest::relate_problem(
                &mut connection,
                contest::NewContestProblems {
                    label: problem_label
                        .get(&problem_id_without_revision)
                        .ok_or_else(|| PageError::Validation(
                            "Arquivo não contém problemas listados".into(),
                        ))?
                        .to_string()
                        .to_uppercase(),
                    contest_id: contest.as_ref().unwrap().id,
                    problem_id,
                },
            )?;
        }
    }

    if form.name.as_ref().unwrap() != "" {
        Ok(redirect_to_referer(
            "Competição criada com sucesso".into(),
            &request,
        ))
    } else {
        Ok(redirect_to_referer(
            "Problemas adicionados com sucesso".into(),
            &request,
        ))
    }
}
