use crate::models::problem::ProblemByContest;
use crate::models::{contest, problem, submission};
use crate::pages::get_editor::{get_formatted_languages, FormattedLanguage};
use crate::pages::prelude::*;
use crate::pages::{
    assert_contest_not_started, get_formatted_contest, get_formatted_submissions, FormattedContest,
    FormattedSubmission,
};
use crate::Language;

#[get("/contests/{id}/{label}")]
async fn get_contest_problem_by_id_label(
    base: BaseContext,
    identity: Identity,
    pool: Data<DbPool>,
    hb: Data<Handlebars<'_>>,
    languages: Data<Arc<DashMap<String, Language>>>,
    session: Session,
    path: Path<(i32, String)>,
    tz: Data<Tz>,
) -> PageResult {
    let logged_user = require_identity(&identity)?;
    let (contest_id, problem_label) = path.into_inner();

    #[derive(Serialize)]
    struct Context {
        base: BaseContext,
        languages: Vec<FormattedLanguage>,
        language: Option<String>,
        contest: FormattedContest,
        problems: Vec<ProblemByContest>,
        problem: ProblemByContest,
        submissions: Vec<FormattedSubmission>,
    }

    let mut connection = pool.get()?;
    let contest = contest::get_contest_by_id(&mut connection, contest_id)?;
    assert_contest_not_started(&logged_user, &contest)?;
    let problems = problem::get_problems_by_contest_id(&mut connection, contest_id)?;
    let problem =
        problem::get_problem_by_contest_id_label(&mut connection, contest_id, &problem_label)?;
    let submissions = submission::get_submissions_user_by_contest_problem(
        &mut connection,
        logged_user.id,
        contest_id,
        &problem_label,
    )?;

    render(
        &hb,
        "contest_problem",
        &Context {
            base,
            contest: get_formatted_contest(&tz, &contest),
            languages: get_formatted_languages(&languages, logged_user.is_admin),
            problems,
            problem,
            language: session.get("language")?,
            submissions: get_formatted_submissions(&tz, &submissions),
        },
    )
}
