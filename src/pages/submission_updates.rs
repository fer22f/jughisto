use crate::pages::prelude::*;

#[get("/submission_updates/")]
async fn submission_updates(broadcaster: Data<Mutex<Broadcaster>>) -> HttpResponse {
    let rx = broadcaster
        .lock()
        .expect("Submission broadcaster is not active")
        .new_client();

    HttpResponse::Ok()
        .append_header(("content-type", "text/event-stream"))
        .streaming(rx)
}
