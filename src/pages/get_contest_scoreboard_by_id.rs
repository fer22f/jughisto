use itertools::Itertools;

use crate::models::{contest, problem, submission};
use crate::pages::prelude::*;
use crate::pages::{
    assert_contest_not_started, get_formatted_contest, get_formatted_problem_by_contest_with_score,
    get_formatted_submissions, FormattedContest, FormattedProblemByContestWithScore,
    FormattedSubmission,
};

#[get("/contests/{id}/scoreboard")]
async fn get_contest_scoreboard_by_id(
    base: BaseContext,
    identity: Identity,
    pool: Data<DbPool>,
    hb: Data<Handlebars<'_>>,
    path: Path<(i32,)>,
    tz: Data<Tz>,
) -> PageResult {
    let logged_user = require_identity(&identity)?;
    let (contest_id,) = path.into_inner();

    #[derive(Serialize)]
    struct Score {
        pub user_name: Option<String>,
        pub problems: Vec<FormattedProblemByContestWithScore>,
        pub solved_count: i64,
        pub penalty: i64,
    }

    #[derive(Serialize)]
    struct Context {
        base: BaseContext,
        contest: FormattedContest,
        scores: Vec<Score>,
        submissions: Vec<FormattedSubmission>,
    }

    let mut connection = pool.get()?;
    let contest = contest::get_contest_by_id(&mut connection, contest_id)?;
    assert_contest_not_started(&logged_user, &contest)?;
    let scores = problem::get_problems_by_contest_id_with_score(&mut connection, contest_id)?;
    let submissions =
        submission::get_submissions_user_by_contest(&mut connection, logged_user.id, contest_id)?;
    let mut scores: Vec<_> = scores.into_iter().group_by(|e| e.user_name.as_ref().cloned()).into_iter().map(|(user_name, problems)| {
    let problems: Vec<_> = problems.map(|p| get_formatted_problem_by_contest_with_score(&p, &contest)).collect();

    Score {
        user_name,
        solved_count: i64::try_from(problems.iter().filter(|p| !p.first_ac_submission_time.is_empty()).count()).unwrap(),
        penalty: problems.iter().filter(|p| !p.first_ac_submission_time.is_empty())
            .map(|p| match p.first_ac_submission_minutes {
                Some(x) if x >= 0 => x,
                _ => 0,
            } + i64::from(20*p.failed_submissions))
            .sum(),
            problems
    }
}).collect();
    scores.sort_by(|a, b| {
        (
            a.user_name != None,
            -a.solved_count,
            a.penalty,
            &a.user_name,
        )
            .cmp(&(
                b.user_name != None,
                -b.solved_count,
                b.penalty,
                &b.user_name,
            ))
    });

    render(
        &hb,
        "scoreboard",
        &Context {
            base,
            contest: get_formatted_contest(&tz, &contest),
            scores,
            submissions: get_formatted_submissions(&tz, &submissions),
        },
    )
}
