use crate::pages::prelude::*;

#[get("/login")]
pub async fn get_login(base: BaseContext, hb: Data<Handlebars<'_>>) -> PageResult {
    #[derive(Serialize)]
    struct Context {
        base: BaseContext,
    }
    render(&hb, "login", &Context { base })
}
