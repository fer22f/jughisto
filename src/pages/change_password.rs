use crate::models::user;
use crate::models::user::PasswordMatched;
use crate::pages::prelude::*;

#[derive(Serialize, Deserialize)]
struct ChangePasswordForm {
    old_password: String,
    new_password: String,
    new_password_repeat: String,
}

#[post("/me/password")]
async fn change_password(
    identity: Identity,
    form: Form<ChangePasswordForm>,
    pool: Data<DbPool>,
    request: HttpRequest,
) -> PageResult {
    let identity = require_identity(&identity)?;
    if form.new_password != form.new_password_repeat {
        return Err(PageError::Validation("Senhas são diferentes".into()));
    }

    let mut connection = pool.get()?;

    match user::change_password(
        &mut connection,
        identity.id,
        &form.old_password,
        &form.new_password,
    )? {
        PasswordMatched::PasswordMatches(_) => Ok(redirect_to_referer(
            "Senha alterada com sucesso".into(),
            &request,
        )),
        _ => Ok(redirect_to_referer(
            "Senha antiga incorreta".into(),
            &request,
        )),
    }
}
