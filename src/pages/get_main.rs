use crate::models::submission;
use crate::pages::prelude::*;
use crate::pages::{
    get_formatted_contests, get_formatted_submissions, FormattedContest, FormattedSubmission,
};

#[get("/")]
async fn get_main(
    base: BaseContext,
    identity: Option<Identity>,
    pool: Data<DbPool>,
    hb: Data<Handlebars<'_>>,
    tz: Data<Tz>,
) -> PageResult {
    let logged_user = get_identity(&identity.as_ref());

    #[derive(Serialize)]
    struct Context {
        base: BaseContext,
        contests: Vec<FormattedContest>,
        submissions: Vec<FormattedSubmission>,
    }

    let mut connection = pool.get()?;
    let submissions = submission::get_submissions(&mut connection)?;

    render(
        &hb,
        "main",
        &Context {
            base,
            contests: get_formatted_contests(&mut connection, logged_user.map(|u| u.id), &tz)?,
            submissions: get_formatted_submissions(&tz, &submissions),
        },
    )
}
