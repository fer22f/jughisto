use crate::models::submission;
use crate::pages::get_submissions::render_submissions;
use crate::pages::prelude::*;

#[get("/submissions/me/")]
async fn get_submissions_me(
    base: BaseContext,
    identity: Identity,
    pool: Data<DbPool>,
    hb: Data<Handlebars<'_>>,
    tz: Data<Tz>,
) -> PageResult {
    let logged_user = require_identity(&identity)?;
    let mut connection = pool.get()?;
    render_submissions(
        base,
        &hb,
        &tz,
        submission::get_submissions_user(&mut connection, logged_user.id)?,
    )
}
