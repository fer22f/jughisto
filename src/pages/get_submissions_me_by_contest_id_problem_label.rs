use crate::models::submission;
use crate::pages::get_submissions::render_submissions;
use crate::pages::prelude::*;

#[get("/submissions/me/contests/{id}/{label}")]
async fn get_submissions_me_by_contest_id_problem_label(
    base: BaseContext,
    identity: Identity,
    pool: Data<DbPool>,
    hb: Data<Handlebars<'_>>,
    path: Path<(i32, String)>,
    tz: Data<Tz>,
) -> PageResult {
    let logged_user = require_identity(&identity)?;
    let mut connection = pool.get()?;
    let (contest_id, problem_label) = path.into_inner();
    render_submissions(
        base,
        &hb,
        &tz,
        submission::get_submissions_user_by_contest_problem(
            &mut connection,
            logged_user.id,
            contest_id,
            &problem_label,
        )?,
    )
}
