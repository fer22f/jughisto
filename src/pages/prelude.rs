pub use std::sync::{Arc, Mutex};

pub use actix_identity::Identity;
pub use actix_session::Session;
pub use actix_web::web::{Data, Form, Path};
pub use actix_web::{get, post, FromRequest, HttpRequest, HttpResponse};
pub use actix_web_flash_messages::FlashMessage;
pub use async_channel::Sender;
pub use chrono_tz::Tz;
pub use dashmap::DashMap;
pub use handlebars::{Handlebars, RenderError};
pub use serde::{Deserialize, Serialize};

pub use crate::broadcaster::Broadcaster;
pub use crate::queue::job_protocol::Job;

pub type DbPool = r2d2::Pool<ConnectionManager<PgConnection>>;

use std::env;
use std::future::Future;
use std::pin::Pin;

use actix_web::dev::Payload;
use actix_web::http::header::{ContentType, HeaderValue};
use actix_web::http::{header, StatusCode};
use actix_web_flash_messages::IncomingFlashMessages;
use diesel::r2d2::ConnectionManager;
use diesel::PgConnection;
use log::error;
use thiserror::Error;

#[derive(Serialize, Deserialize, Clone)]
pub struct LoggedUser {
    pub id: i32,
    pub name: String,
    pub is_admin: bool,
}

impl actix_web::error::ResponseError for PageError {
    fn error_response(&self) -> HttpResponse {
        error!("{}", self);
        HttpResponse::build(self.status_code())
            .insert_header(ContentType::plaintext())
            .body(self.to_string())
    }

    fn status_code(&self) -> StatusCode {
        match *self {
            PageError::Unauthorized() => StatusCode::UNAUTHORIZED,
            PageError::Validation(_) => StatusCode::BAD_REQUEST,
            PageError::Forbidden(_) => StatusCode::FORBIDDEN,
            PageError::Custom(_)
            | PageError::SessionInsert(_)
            | PageError::ConnectionPool(_)
            | PageError::Web(_)
            | PageError::Render(_)
            | PageError::Queue(_)
            | PageError::SessionGet(_)
            | PageError::Database(_)
            | PageError::Io(_)
            | PageError::UserHashing(_)
            | PageError::Zip(_) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

#[derive(Error, Debug)]
pub enum PageError {
    #[error("Unauthorized")]
    Unauthorized(),
    #[error("Couldn't render: {0}")]
    Render(#[from] handlebars::RenderError),
    #[error(transparent)]
    SessionGet(#[from] actix_session::SessionGetError),
    #[error("{0}")]
    Custom(String),
    #[error("{0}")]
    Forbidden(String),
    #[error("{0}")]
    Validation(String),
    #[error("Couldn't get connection from pool")]
    ConnectionPool(#[from] r2d2::Error),
    #[error("Couldn't hash")]
    UserHashing(#[from] crate::user::UserHashingError),
    #[error(transparent)]
    Web(#[from] actix_web::Error),
    #[error(transparent)]
    Queue(#[from] async_channel::SendError<Job>),
    #[error("Couldn't fetch result from database")]
    Database(#[from] diesel::result::Error),
    #[error("couldn't insert session")]
    SessionInsert(#[from] actix_session::SessionInsertError),
    #[error("couldn't work with the filesystem")]
    Io(#[from] std::io::Error),
    #[error("couldn't work with the zip")]
    Zip(#[from] zip::result::ZipError),
}

pub fn get_identity(identity: &Option<&Identity>) -> Option<LoggedUser> {
    identity.as_ref().and_then(|identity| {
        let identity = identity.id();
        identity
            .ok()
            .and_then(|identity| serde_json::from_str(&identity).ok())
    })
}

pub fn require_identity(identity: &Identity) -> Result<LoggedUser, PageError> {
    get_identity(&Some(identity)).ok_or(PageError::Unauthorized())
}

pub type PageResult = Result<HttpResponse, PageError>;

#[derive(Serialize)]
pub struct BaseContext {
    logged_user: Option<LoggedUser>,
    flash_messages: IncomingFlashMessages,
    base_url: String,
}

impl FromRequest for BaseContext {
    type Error = actix_web::Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self, Self::Error>>>>;

    fn from_request(req: &HttpRequest, _: &mut Payload) -> Self::Future {
        let req = req.clone();
        Box::pin(async move {
            let identity = Option::<Identity>::from_request(&req, &mut Payload::None).await;
            let logged_user = identity?.and_then(|identity| get_identity(&Some(&identity)));
            let flash_messages =
                IncomingFlashMessages::from_request(&req, &mut Payload::None).await?;
            Ok(BaseContext {
                logged_user,
                flash_messages,
                base_url: env::var("BASE_URL").expect("BASE_URL environment variable is not set"),
            })
        })
    }
}

pub fn render<Err: From<RenderError>, Ctx: serde::Serialize>(
    hb: &Handlebars,
    name: &str,
    context: &Ctx,
) -> Result<HttpResponse, Err> {
    Ok(HttpResponse::Ok().body(hb.render(name, context)?))
}

pub fn redirect_to_referer(message: String, request: &HttpRequest) -> HttpResponse {
    let referer = request
        .headers()
        .get("Referer")
        .and_then(|h| h.to_str().ok()).map_or_else(|| env::var("BASE_URL").expect("BASE_URL environment variable is not set"), std::convert::Into::into);
    FlashMessage::info(message).send();
    HttpResponse::SeeOther()
        .append_header((header::LOCATION, HeaderValue::from_str(&referer).unwrap()))
        .finish()
}

pub fn redirect_to_root() -> HttpResponse {
    HttpResponse::SeeOther()
        .append_header((
            header::LOCATION,
            HeaderValue::from_str(
                &env::var("BASE_URL").expect("BASE_URL environment variable is not set"),
            )
            .unwrap(),
        ))
        .finish()
}
