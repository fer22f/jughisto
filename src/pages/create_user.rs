use chrono::Local;

use crate::models::user;
use crate::pages::prelude::*;

#[derive(Serialize, Deserialize)]
struct CreateUserForm {
    name: String,
    password: String,
    is_admin: Option<bool>,
}

#[post("/users/")]
async fn create_user(
    identity: Identity,
    pool: Data<DbPool>,
    form: Form<CreateUserForm>,
    request: HttpRequest,
) -> PageResult {
    let identity = require_identity(&identity)?;
    if !identity.is_admin {
        return Err(PageError::Forbidden(
            "Apenas administradores podem fazer isso".into(),
        ));
    }

    let mut connection = pool.get()?;

    user::insert_new_user(
        &mut connection,
        user::NewUser {
            name: &form.name,
            password: &form.password,
            is_admin: form.is_admin.unwrap_or(false),
            creation_instant: Local::now().naive_utc(),
            creation_user_id: Some(identity.id),
        },
    )?;

    Ok(redirect_to_referer(
        "Usuário criado com sucesso".into(),
        &request,
    ))
}
