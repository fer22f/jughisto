use crate::models::{problem, submission};
use crate::pages::prelude::*;

#[post("/submissions/{uuid}/rejudge")]
async fn rejudge_submission(
    identity: Identity,
    pool: Data<DbPool>,
    job_sender: Data<Sender<Job>>,
    request: HttpRequest,
    path: Path<(String,)>,
) -> PageResult {
    let logged_user = require_identity(&identity)?;
    let mut connection = pool.get()?;

    if !logged_user.is_admin {
        return Err(PageError::Forbidden(
            "Apenas administradores podem fazer isso".into(),
        ));
    }

    let (submission_uuid,) = path.into_inner();
    let (submission, _, _, _) =
        submission::get_submission_by_uuid(&mut connection, submission_uuid.clone())?;
    let metadata = problem::get_problem_by_contest_id_metadata(
        &mut connection,
        submission.contest_problem_id,
    )?;

    job_sender
        .send(crate::create_job_from_submission(submission, metadata))
        .await?;

    Ok(redirect_to_referer(
        format!("Rejulgando {}", submission_uuid),
        &request,
    ))
}
