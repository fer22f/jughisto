use crate::models::submission;
use crate::pages::prelude::*;

#[get("/submissions/{uuid}")]
async fn get_submission(
    base: BaseContext,
    identity: Identity,
    pool: Data<DbPool>,
    hb: Data<Handlebars<'_>>,
    path: Path<(String,)>,
) -> PageResult {
    let logged_user = require_identity(&identity)?;
    let (submission_uuid,) = path.into_inner();
    let mut connection = pool.get()?;

    #[derive(Serialize)]
    struct Submission {
        pub uuid: String,
        pub verdict: Option<String>,
        pub source_text: String,
        pub language: String,
        pub memory_kib: Option<i32>,
        pub time_ms: Option<i32>,
        pub time_wall_ms: Option<i32>,
        pub error_output: Option<String>,
        pub user_name: String,
        pub problem_label: String,
        pub contest_name: String,
        pub failed_test: Option<i32>,
    }

    #[derive(Serialize)]
    struct Context {
        base: BaseContext,
        submission: Submission,
    }
    let (submission, user, contest_problem, contest) =
        submission::get_submission_by_uuid(&mut connection, submission_uuid)?;

    if user.id != logged_user.id && !logged_user.is_admin {
        return Err(PageError::Forbidden(
            "Não é possível acessar uma submissão de outro usuário".into(),
        ));
    }

    render(
        &hb,
        "submission",
        &Context {
            base,
            submission: Submission {
                uuid: submission.uuid,
                verdict: submission.verdict,
                source_text: submission.source_text,
                language: submission.language,
                memory_kib: submission.memory_kib,
                time_ms: submission.time_ms,
                time_wall_ms: submission.time_wall_ms,
                error_output: submission.error_output,
                user_name: user.name,
                problem_label: contest_problem.label,
                contest_name: contest.name,
                failed_test: submission.failed_test,
            },
        },
    )
}
