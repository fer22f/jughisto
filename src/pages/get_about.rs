use crate::pages::prelude::*;

#[get("/about")]
async fn get_about(base: BaseContext, hb: Data<Handlebars<'_>>) -> PageResult {
    #[derive(Serialize)]
    struct Context {
        base: BaseContext
    }

    render(&hb, "about", &Context { base })
}
