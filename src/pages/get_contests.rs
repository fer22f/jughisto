use crate::pages::prelude::*;
use crate::pages::{get_formatted_contests, FormattedContest};

#[get("/contests/")]
async fn get_contests(
    base: BaseContext,
    identity: Identity,
    pool: Data<DbPool>,
    hb: Data<Handlebars<'_>>,
    tz: Data<Tz>,
) -> PageResult {
    let logged_user = require_identity(&identity)?;

    #[derive(Serialize)]
    struct Context {
        base: BaseContext,
        contests: Vec<FormattedContest>,
    }

    let mut connection = pool.get()?;
    render(
        &hb,
        "contests",
        &Context {
            base,
            contests: get_formatted_contests(&mut connection, Some(logged_user.id), &tz)?,
        },
    )
}
