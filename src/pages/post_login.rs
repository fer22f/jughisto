use actix_web::{web, HttpMessage};

use crate::models::user;
use crate::models::user::{PasswordMatched, UserHashingError};
use crate::pages::prelude::*;

#[derive(Serialize, Deserialize)]
struct LoginForm {
    name: String,
    password: String,
}

#[post("/login")]
async fn post_login(pool: Data<DbPool>, form: Form<LoginForm>, request: HttpRequest) -> PageResult {
    let mut connection = pool.get()?;

    match web::block(move || {
        user::check_matching_password(&mut connection, &form.name, &form.password)
    })
    .await
    .map_err(|e| PageError::Web(e.into()))?
    .map_err(|e| match e {
        UserHashingError::Database(e) => PageError::Database(e),
        UserHashingError::Hash(_) => PageError::Validation("Senha inválida".into()),
    })? {
        PasswordMatched::UserDoesntExist => {
            Err(PageError::Validation("Usuário inexistente".into()))
        }
        PasswordMatched::PasswordDoesntMatch => {
            Err(PageError::Validation("Senha incorreta".into()))
        }
        PasswordMatched::PasswordMatches(logged_user) => {
            Identity::login(
                &request.extensions(),
                serde_json::to_string(&LoggedUser {
                    id: logged_user.id,
                    name: (&logged_user.name).into(),
                    is_admin: logged_user.is_admin,
                })
                .map_err(|_| PageError::Custom("Usuário no banco de dados inconsistente".into()))?,
            )
            .map_err(|_| PageError::Custom("Impossível fazer login".into()))?;
            Ok(redirect_to_root())
        }
    }
}
