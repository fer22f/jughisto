use crate::models::{contest, problem, submission};
use crate::pages::prelude::*;
use crate::pages::{
    assert_contest_not_started, get_formatted_contest, get_formatted_problem_by_contest_with_score,
    get_formatted_submissions, FormattedContest, FormattedProblemByContestWithScore,
    FormattedSubmission,
};

#[get("/contests/{id}")]
pub async fn get_contest_by_id(
    base: BaseContext,
    identity: Identity,
    pool: Data<DbPool>,
    hb: Data<Handlebars<'_>>,
    path: Path<(i32,)>,
    tz: Data<Tz>,
) -> PageResult {
    let logged_user = require_identity(&identity)?;
    let (contest_id,) = path.into_inner();

    #[derive(Serialize)]
    struct Context {
        base: BaseContext,
        contest: FormattedContest,
        problems: Vec<FormattedProblemByContestWithScore>,
        submissions: Vec<FormattedSubmission>,
    }

    let mut connection = pool.get()?;
    let contest = contest::get_contest_by_id(&mut connection, contest_id)?;
    assert_contest_not_started(&logged_user, &contest)?;

    let problems = problem::get_problems_user_by_contest_id_with_score(
        &mut connection,
        logged_user.id,
        contest_id,
    )?;
    let submissions =
        submission::get_submissions_user_by_contest(&mut connection, logged_user.id, contest_id)?;

    render(
        &hb,
        "contest",
        &Context {
            base,
            contest: get_formatted_contest(&tz, &contest),
            problems: problems
                .iter()
                .map(|p| get_formatted_problem_by_contest_with_score(p, &contest))
                .collect(),
            submissions: get_formatted_submissions(&tz, &submissions),
        },
    )
}
