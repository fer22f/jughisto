use std::path::PathBuf;

use actix_files::NamedFile;

use crate::models::{contest, problem};
use crate::pages::assert_contest_not_started;
use crate::pages::prelude::*;

#[get("/problems/{id}/assets/{filename}")]
async fn get_problem_by_id_assets(
    identity: Identity,
    pool: Data<DbPool>,
    path: Path<(i32, String)>,
) -> Result<NamedFile, PageError> {
    let logged_user = require_identity(&identity)?;
    let (problem_id, asset_filename) = path.into_inner();
    let mut connection = pool.get()?;
    let problem = problem::get_problem_by_contest_id_metadata(&mut connection, problem_id)?;
    let contest = contest::get_contest_by_id(&mut connection, problem.contest_id)?;
    assert_contest_not_started(&logged_user, &contest)?;
    let file_path = PathBuf::from("/data/")
        .join(problem.id)
        .join("statements/.html/portuguese/")
        .join(asset_filename);
    Ok(NamedFile::open(file_path)?)
}
