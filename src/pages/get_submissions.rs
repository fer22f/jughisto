use submission::{ContestProblem, Submission};

use crate::models::submission;
use crate::pages::prelude::*;
use crate::pages::{get_formatted_submissions, FormattedSubmission};
use crate::user::User;

pub fn render_submissions(
    base: BaseContext,
    hb: &Handlebars<'_>,
    tz: &Tz,
    submissions: Vec<(Submission, ContestProblem, User)>,
) -> PageResult {
    #[derive(Serialize)]
    struct Context {
        base: BaseContext,
        submissions: Vec<FormattedSubmission>,
    }

    render(
        hb,
        "submissions",
        &Context {
            base,
            submissions: get_formatted_submissions(tz, &submissions),
        },
    )
}

#[get("/submissions/")]
async fn get_submissions(
    base: BaseContext,
    identity: Identity,
    pool: Data<DbPool>,
    hb: Data<Handlebars<'_>>,
    tz: Data<Tz>,
) -> PageResult {
    let logged_user = require_identity(&identity)?;
    let mut connection = pool.get()?;
    render_submissions(
        base,
        &hb,
        &tz,
        if logged_user.is_admin {
            submission::get_submissions(&mut connection)?
        } else {
            submission::get_submissions_user(&mut connection, logged_user.id)?
        },
    )
}
