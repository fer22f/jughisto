use crate::models::problem;
use crate::pages::prelude::*;
use crate::pages::FormattedProblemByContestWithScore;

#[get("/problems/")]
pub async fn get_problems(
    base: BaseContext,
    identity: Identity,
    pool: Data<DbPool>,
    hb: Data<Handlebars<'_>>,
) -> PageResult {
    let logged_user = require_identity(&identity)?;

    #[derive(Serialize)]
    struct Context {
        base: BaseContext,
        problems: Vec<FormattedProblemByContestWithScore>,
    }

    let mut connection = pool.get()?;
    let problems = problem::get_problems_user_with_score(&mut connection, logged_user.id)?;

    render(
        &hb,
        "problems",
        &Context {
            base,
            problems: problems
                .iter()
                .map(|p| FormattedProblemByContestWithScore {
                    first_ac_submission_time: "".into(),
                    first_ac_submission_minutes: None,
                    failed_submissions: p.failed_submissions,
                    id: p.id,
                    name: p.name.clone(),
                    label: p.label.clone(),
                    memory_limit_mib: p.memory_limit_bytes / 1_024 / 1_024,
                    time_limit: format!("{}", f64::from(p.time_limit_ms) / 1000.0)
                        .replacen('.', ",", 1),
                    user_accepted_count: p.user_accepted_count,
                })
                .collect(),
        },
    )
}
