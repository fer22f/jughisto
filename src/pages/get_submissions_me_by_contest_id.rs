use crate::models::submission;
use crate::pages::get_submissions::render_submissions;
use crate::pages::prelude::*;

#[get("/submissions/me/contests/{id}")]
async fn get_submissions_me_by_contest_id(
    base: BaseContext,
    identity: Identity,
    pool: Data<DbPool>,
    hb: Data<Handlebars<'_>>,
    path: Path<(i32,)>,
    tz: Data<Tz>,
) -> PageResult {
    let logged_user = require_identity(&identity)?;
    let (contest_id,) = path.into_inner();
    let mut connection = pool.get()?;

    render_submissions(
        base,
        &hb,
        &tz,
        submission::get_submissions_user_by_contest(&mut connection, logged_user.id, contest_id)?,
    )
}
