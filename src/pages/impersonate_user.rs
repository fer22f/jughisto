use actix_web::HttpMessage;

use crate::models::user;
use crate::pages::prelude::*;

#[derive(Serialize, Deserialize)]
struct ImpersonateUserForm {
    name: String,
}

#[post("/impersonate/")]
async fn impersonate_user(
    identity: Identity,
    pool: Data<DbPool>,
    form: Form<ImpersonateUserForm>,
    request: HttpRequest,
) -> PageResult {
    let my_identity = require_identity(&identity)?;
    if !my_identity.is_admin {
        return Err(PageError::Forbidden(
            "Apenas administradores podem fazer isso".into(),
        ));
    }

    let mut connection = pool.get()?;

    let user = user::get_user_by_name(&mut connection, &form.name)?;
    Identity::login(
        &request.extensions(),
        serde_json::to_string(&LoggedUser {
            id: user.id,
            name: (&user.name).into(),
            is_admin: user.is_admin,
        })
        .map_err(|_| PageError::Custom("Usuário no banco de dados inconsistente".into()))?,
    )
    .map_err(|_| PageError::Custom("Impossível fazer login".into()))?;

    Ok(redirect_to_referer(
        "Personificado com sucesso".into(),
        &request,
    ))
}
