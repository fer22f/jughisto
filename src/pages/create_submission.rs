use chrono::Local;
use uuid::Uuid;

use crate::models::{contest, problem, submission};
use crate::pages::assert_contest_not_started;
use crate::pages::prelude::*;
use crate::queue::job_protocol::{job, Job, Language};

#[derive(Serialize, Deserialize)]
struct SubmissionForm {
    contest_problem_id: i32,
    language: String,
    source_text: String,
}

#[post("/submissions/")]
async fn create_submission(
    identity: Identity,
    form: Form<SubmissionForm>,
    pool: Data<DbPool>,
    job_sender: Data<Sender<Job>>,
    languages: Data<Arc<DashMap<String, Language>>>,
    session: Session,
    request: HttpRequest,
) -> PageResult {
    let logged_user = require_identity(&identity)?;
    let mut connection = pool.get()?;

    languages
        .get(&form.language)
        .ok_or_else(|| PageError::Validation("Linguagem inexistente".into()))?;

    if !logged_user.is_admin && form.language != "cpp.20.g++" {
        return Err(PageError::Validation(
            "Somente é possível submeter em C++".into(),
        ));
    }

    let uuid = Uuid::new_v4();
    submission::insert_submission(
        &mut connection,
        submission::NewSubmission {
            uuid: uuid.to_string(),
            source_text: (&form.source_text).into(),
            language: (&form.language).into(),
            submission_instant: Local::now().naive_utc(),
            contest_problem_id: form.contest_problem_id,
            user_id: logged_user.id,
        },
    )?;

    let contest =
        contest::get_contest_by_contest_problem_id(&mut connection, form.contest_problem_id)?;
    assert_contest_not_started(&logged_user, &contest)?;

    let metadata =
        problem::get_problem_by_contest_id_metadata(&mut connection, form.contest_problem_id)?;

    job_sender
        .send(Job {
            uuid: uuid.to_string(),
            language: (&form.language).into(),
            time_limit_ms: metadata.time_limit_ms,
            memory_limit_kib: metadata.memory_limit_bytes / 1_024,

            which: Some(job::Which::Judgement(job::Judgement {
                source_text: (&form.source_text).into(),
                test_count: metadata.test_count,
                test_pattern: format!("./{}/{}", metadata.id, metadata.test_pattern),
                checker_language: metadata.checker_language,
                checker_source_path: format!("./{}/{}", metadata.id, metadata.checker_path),
            })),
        })
        .await?;

    session.insert("language", &form.language)?;
    Ok(redirect_to_referer(
        format!("Submetido {} com sucesso!", uuid),
        &request,
    ))
}
