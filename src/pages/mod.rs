use std::env;

use actix_web::dev;
use actix_web::http::header::HeaderValue;
use actix_web::http::{header, StatusCode};
use actix_web::middleware::ErrorHandlerResponse;
use actix_web_flash_messages::FlashMessage;
use chrono::{Local, NaiveDateTime, TimeZone};
use chrono_tz::Tz;
use contest::{Contest, ContestWithAcs};
use diesel::pg::PgConnection;
use problem::ProblemByContestWithScore;
use submission::{ContestProblem, Submission};

use crate::models::{contest, problem, submission};
use crate::user::User;

pub mod change_password;
pub mod create_contest;
pub mod create_submission;
pub mod create_user;
pub mod get_about;
pub mod get_contest_by_id;
pub mod get_contest_problem_by_id_label;
pub mod get_contest_scoreboard_by_id;
pub mod get_contests;
pub mod get_editor;
pub mod get_login;
pub mod get_main;
pub mod get_me;
pub mod get_problem_by_id_assets;
pub mod get_problems;
pub mod get_submission;
pub mod get_submissions;
pub mod get_submissions_me;
pub mod get_submissions_me_by_contest_id;
pub mod get_submissions_me_by_contest_id_problem_label;
pub mod impersonate_user;
pub mod post_login;
pub mod post_logout;
pub mod prelude;
pub mod rejudge_submission;
pub mod submission_updates;

use prelude::*;

pub fn render_401<B>(
    mut res: dev::ServiceResponse<B>,
) -> actix_web::Result<ErrorHandlerResponse<B>> {
    FlashMessage::error("Você precisa estar logado para acessar esta página").send();
    res.response_mut().headers_mut().insert(
        header::LOCATION,
        HeaderValue::from_str(&format!(
            "{}login",
            &env::var("BASE_URL").expect("BASE_URL environment variable is not set")
        ))
        .unwrap(),
    );
    *res.response_mut().status_mut() = StatusCode::SEE_OTHER;
    Ok(ErrorHandlerResponse::Response(res.map_into_left_body()))
}

pub fn render_400<B>(res: dev::ServiceResponse<B>) -> actix_web::Result<ErrorHandlerResponse<B>> {
    FlashMessage::error("Entrada inválida").send();
    Ok(ErrorHandlerResponse::Response(res.map_into_left_body()))
}

fn format_duration(duration: chrono::Duration) -> String {
    format!(
        "{}{}{:02}:{:02}",
        if duration.num_milliseconds() >= 0 {
            ""
        } else {
            "-"
        },
        if duration.num_days() != 0 {
            format!("{}:", duration.num_days().abs())
        } else {
            "".into()
        },
        duration.num_hours().abs() % 24,
        duration.num_minutes().abs() % 60
    )
}

#[derive(Serialize)]
struct FormattedProblemByContestWithScore {
    pub first_ac_submission_time: String,
    pub first_ac_submission_minutes: Option<i64>,
    pub user_accepted_count: i32,
    pub failed_submissions: i32,
    pub id: i32,
    pub name: String,
    pub label: String,
    pub memory_limit_mib: i32,
    pub time_limit: String,
}

fn get_formatted_problem_by_contest_with_score(
    p: &ProblemByContestWithScore,
    contest: &Contest,
) -> FormattedProblemByContestWithScore {
    FormattedProblemByContestWithScore {
        first_ac_submission_time: p
            .first_ac_submission_instant
            .map(|t| match contest.start_instant {
                Some(cs) => format_duration(t - cs),
                None => "*".into(),
            })
            .unwrap_or_else(|| "".into()),
        first_ac_submission_minutes: p.first_ac_submission_instant.and_then(|t| {
            contest.start_instant.map(|cs| (t - cs).num_minutes())
        }),
        failed_submissions: p.failed_submissions,
        id: p.id,
        name: p.name.clone(),
        label: p.label.clone(),
        memory_limit_mib: p.memory_limit_bytes / 1_024 / 1_024,
        time_limit: format!("{}", f64::from(p.time_limit_ms) / 1000.0).replacen('.', ",", 1),
        user_accepted_count: p.user_accepted_count,
    }
}

fn assert_contest_not_started(
    logged_user: &LoggedUser,
    contest: &Contest,
) -> Result<(), PageError> {
    if contest
        .start_instant
        .map_or(false, |s| s > Local::now().naive_utc())
        && !logged_user.is_admin
    {
        return Err(PageError::Forbidden(
            "Essa competição ainda não começou".into(),
        ));
    }
    Ok(())
}

#[derive(Serialize)]
struct FormattedSubmission {
    uuid: String,
    verdict: String,
    problem_label: String,
    submission_instant: String,
    error_output: Option<String>,
    user_name: String,
    time_ms: Option<i32>,
    memory: String,
    failed_test: Option<i32>,
}

fn format_utc_date_time(tz: &Tz, input: NaiveDateTime) -> String {
    tz.from_utc_datetime(&input)
        .format("%d/%m/%Y %H:%M:%S")
        .to_string()
}

fn get_formatted_submissions(
    tz: &Tz,
    vec: &[(Submission, ContestProblem, User)],
) -> Vec<FormattedSubmission> {
    vec.iter()
        .map(|(submission, contest_problem, user)| FormattedSubmission {
            uuid: (&submission.uuid).into(),
            verdict: submission
                .verdict
                .as_ref().map_or_else(|| "WJ".into(), String::from),
            problem_label: contest_problem.label.clone(),
            submission_instant: format_utc_date_time(tz, submission.submission_instant),
            error_output: submission.error_output.as_ref().map(std::convert::Into::into),
            user_name: user.name.clone(),
            time_ms: submission.time_ms,
            memory: match submission.memory_kib {
                None | Some(0) => "".into(),
                Some(k) if k < 1_024 => format!("{}KiB", k),
                Some(k) => format!("{}MiB", k / 1_024),
            },
            failed_test: submission.failed_test,
        })
        .collect()
}

#[derive(Serialize)]
struct FormattedContest {
    pub id: i32,
    pub name: String,
    pub start_instant: Option<String>,
    pub end_instant: Option<String>,
    pub creation_instant: String,
    pub grade_ratio: Option<i32>,
    pub grade_after_ratio: Option<i32>,
    pub accepted_count: i32,
    pub accepted_after_count: i32,
    pub accepted_total_count: i32,
    pub problem_count: i32,
    pub grade: String,
}

fn get_formatted_contest(tz: &Tz, contest: &Contest) -> FormattedContest {
    FormattedContest {
        id: contest.id,
        name: contest.name.clone(),
        start_instant: contest.start_instant.map(|i| format_utc_date_time(tz, i)),
        end_instant: contest.end_instant.map(|i| format_utc_date_time(tz, i)),
        creation_instant: format_utc_date_time(tz, contest.creation_instant),
        grade_ratio: contest.grade_ratio,
        grade_after_ratio: contest.grade_after_ratio,
        accepted_count: 0,
        accepted_after_count: 0,
        accepted_total_count: 0,
        problem_count: 0,
        grade: "".into(),
    }
}

fn get_formatted_contest_acs(tz: &Tz, contest: &ContestWithAcs) -> FormattedContest {
    FormattedContest {
        id: contest.id,
        name: contest.name.clone(),
        start_instant: contest.start_instant.map(|i| format_utc_date_time(tz, i)),
        end_instant: contest.end_instant.map(|i| format_utc_date_time(tz, i)),
        creation_instant: format_utc_date_time(tz, contest.creation_instant),
        grade_ratio: contest.grade_ratio,
        grade_after_ratio: contest.grade_after_ratio,
        accepted_count: contest.accepted_count,
        accepted_after_count: contest.accepted_after_count,
        accepted_total_count: contest.accepted_count + contest.accepted_after_count,
        problem_count: contest.problem_count,
        grade: match contest.grade_ratio {
            Some(grade_ratio) => format!(
                "{:.2}",
                10.0 * (f64::from(contest.accepted_count) * 1.0 / f64::from(grade_ratio)
                    + match contest.grade_after_ratio {
                        Some(grade_after_ratio) =>
                            f64::from(contest.accepted_after_count) * 1.0
                                / f64::from(grade_after_ratio),
                        None => 0.0,
                    })
            )
            .replacen('.', ",", 1),
            None => "".into(),
        },
    }
}

fn get_formatted_contests(
    connection: &mut PgConnection,
    user_id: Option<i32>,
    tz: &Tz,
) -> Result<Vec<FormattedContest>, PageError> {
    Ok(match user_id {
        Some(user_id) => contest::get_contests_with_acs(connection, user_id)?
            .iter()
            .map(|c| get_formatted_contest_acs(tz, c))
            .collect(),
        None => contest::get_contests(connection)?
            .iter()
            .map(|c| get_formatted_contest(tz, c))
            .collect(),
    })
}
