use crate::pages::prelude::*;
use crate::Language;

#[derive(Serialize, Debug)]
pub struct FormattedLanguage {
    order: i32,
    name: String,
    value: String,
}

pub fn get_formatted_languages(
    languages: &DashMap<String, Language>,
    allow_all_languages: bool,
) -> Vec<FormattedLanguage> {
    let mut languages = languages
        .iter()
        .filter(|kv| {
            if allow_all_languages {
                true
            } else {
                kv.key() == "cpp.20.g++"
            }
        })
        .map(|kv| FormattedLanguage {
            order: kv.value().order,
            value: kv.key().into(),
            name: kv.value().name.clone(),
        })
        .collect::<Vec<_>>();
    languages.sort_by(|a, b| a.order.cmp(&b.order));
    languages
}

#[get("/editor")]
async fn get_editor(
    base: BaseContext,
    hb: Data<Handlebars<'_>>,
    identity: Identity,
    session: Session,
    languages: Data<Arc<DashMap<String, Language>>>,
) -> PageResult {
    let logged_user = require_identity(&identity)?;

    #[derive(Serialize)]
    struct Context {
        base: BaseContext,
        languages: Vec<FormattedLanguage>,
        language: Option<String>,
    }

    render(
        &hb,
        "editor",
        &Context {
            base,
            languages: get_formatted_languages(&languages, logged_user.is_admin),
            language: session.get("language")?,
        },
    )
}
