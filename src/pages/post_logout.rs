use crate::pages::prelude::*;

#[post("/logout")]
async fn post_logout(identity: Identity) -> PageResult {
    identity.logout();
    Ok(redirect_to_root())
}
