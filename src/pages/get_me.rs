use crate::pages::prelude::*;

#[get("/me")]
async fn get_me(base: BaseContext, identity: Identity, hb: Data<Handlebars<'_>>) -> PageResult {
    require_identity(&identity)?;
    #[derive(Serialize)]
    struct Context {
        base: BaseContext,
    }
    render(&hb, "me", &Context { base })
}
