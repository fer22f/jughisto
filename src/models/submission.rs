use chrono::prelude::*;
use diesel::insert_into;
use diesel::prelude::*;

use crate::contest::{Contest, CONTEST_COLUMNS};
use crate::schema::{contest, contest_problems, problem, submission, user};
use crate::user::{User, USER_COLUMNS};

#[derive(Queryable)]
pub struct Submission {
    pub uuid: String,
    pub verdict: Option<String>,
    pub source_text: String,
    pub language: String,
    pub submission_instant: NaiveDateTime,
    pub judge_start_instant: Option<NaiveDateTime>,
    pub judge_end_instant: Option<NaiveDateTime>,
    pub memory_kib: Option<i32>,
    pub time_ms: Option<i32>,
    pub time_wall_ms: Option<i32>,
    pub error_output: Option<String>,
    pub contest_problem_id: i32,
    pub user_id: i32,
    pub failed_test: Option<i32>,
}

#[allow(clippy::type_complexity)]
const SUBMISSION_COLUMNS: (
    submission::uuid,
    submission::verdict,
    submission::source_text,
    submission::language,
    submission::submission_instant,
    submission::judge_start_instant,
    submission::judge_end_instant,
    submission::memory_kib,
    submission::time_ms,
    submission::time_wall_ms,
    submission::error_output,
    submission::contest_problem_id,
    submission::user_id,
    submission::failed_test,
) = (
    submission::uuid,
    submission::verdict,
    submission::source_text,
    submission::language,
    submission::submission_instant,
    submission::judge_start_instant,
    submission::judge_end_instant,
    submission::memory_kib,
    submission::time_ms,
    submission::time_wall_ms,
    submission::error_output,
    submission::contest_problem_id,
    submission::user_id,
    submission::failed_test,
);

#[derive(Queryable)]
pub struct ContestProblem {
    pub id: i32,
    pub label: String,
    pub contest_id: i32,
    pub problem_id: String,
}

const CONTEST_PROBLEMS_COLUMNS: (
    contest_problems::id,
    contest_problems::label,
    contest_problems::contest_id,
    contest_problems::problem_id,
) = (
    contest_problems::id,
    contest_problems::label,
    contest_problems::contest_id,
    contest_problems::problem_id,
);

#[derive(Insertable)]
#[diesel(table_name = submission)]
pub struct NewSubmission {
    pub uuid: String,
    pub source_text: String,
    pub language: String,
    pub submission_instant: NaiveDateTime,
    pub contest_problem_id: i32,
    pub user_id: i32,
}

pub fn insert_submission(
    connection: &mut PgConnection,
    new_submission: NewSubmission,
) -> QueryResult<()> {
    insert_into(submission::table)
        .values(new_submission)
        .execute(connection)?;
    Ok(())
}

pub struct SubmissionCompletion {
    pub uuid: String,
    pub verdict: String,
    pub judge_start_instant: NaiveDateTime,
    pub judge_end_instant: NaiveDateTime,
    pub memory_kib: Option<i32>,
    pub time_ms: Option<i32>,
    pub time_wall_ms: Option<i32>,
    pub error_output: Option<String>,
    pub failed_test: Option<i32>,
}

pub fn complete_submission(
    connection: &mut PgConnection,
    submission: SubmissionCompletion,
) -> QueryResult<()> {
    diesel::update(submission::table)
        .filter(submission::uuid.eq(submission.uuid))
        .set((
            submission::verdict.eq(submission.verdict),
            submission::judge_start_instant.eq(submission.judge_start_instant),
            submission::judge_end_instant.eq(submission.judge_end_instant),
            submission::memory_kib.eq(submission.memory_kib),
            submission::time_ms.eq(submission.time_ms),
            submission::time_wall_ms.eq(submission.time_wall_ms),
            submission::error_output.eq(submission.error_output),
            submission::failed_test.eq(submission.failed_test),
        ))
        .execute(connection)?;
    Ok(())
}

use crate::problem::ProblemByContestMetadata;

pub fn get_waiting_judge_submissions(
    connection: &mut PgConnection,
) -> QueryResult<Vec<(Submission, ProblemByContestMetadata)>> {
    submission::table
        .inner_join(contest_problems::table.inner_join(problem::table))
        .order_by(submission::submission_instant.asc())
        .select((
            SUBMISSION_COLUMNS,
            (
                contest_problems::contest_id,
                problem::id,
                problem::memory_limit_bytes,
                problem::time_limit_ms,
                problem::checker_path,
                problem::checker_language,
                problem::validator_path,
                problem::validator_language,
                problem::main_solution_path,
                problem::main_solution_language,
                problem::test_count,
                problem::test_pattern,
                problem::status,
            ),
        ))
        .filter(submission::verdict.is_null())
        .load::<(Submission, ProblemByContestMetadata)>(connection)
}

pub fn get_submissions(
    connection: &mut PgConnection,
) -> QueryResult<Vec<(Submission, ContestProblem, User)>> {
    submission::table
        .inner_join(contest_problems::table)
        .inner_join(user::table)
        .order_by(submission::submission_instant.desc())
        .select((SUBMISSION_COLUMNS, CONTEST_PROBLEMS_COLUMNS, USER_COLUMNS))
        .limit(100)
        .load::<(Submission, ContestProblem, User)>(connection)
}

pub fn get_submission_by_uuid(
    connection: &mut PgConnection,
    uuid: String,
) -> QueryResult<(Submission, User, ContestProblem, Contest)> {
    submission::table
        .filter(submission::uuid.eq(uuid))
        .inner_join(contest_problems::table.inner_join(contest::table))
        .inner_join(user::table)
        .select((
            SUBMISSION_COLUMNS,
            USER_COLUMNS,
            CONTEST_PROBLEMS_COLUMNS,
            CONTEST_COLUMNS,
        ))
        .first::<(Submission, User, ContestProblem, Contest)>(connection)
}

pub fn get_submissions_user(
    connection: &mut PgConnection,
    user_id: i32,
) -> QueryResult<Vec<(Submission, ContestProblem, User)>> {
    submission::table
        .filter(submission::user_id.eq(user_id))
        .inner_join(contest_problems::table)
        .inner_join(user::table)
        .order_by(submission::submission_instant.desc())
        .select((SUBMISSION_COLUMNS, CONTEST_PROBLEMS_COLUMNS, USER_COLUMNS))
        .load::<(Submission, ContestProblem, User)>(connection)
}

pub fn get_submissions_user_by_contest(
    connection: &mut PgConnection,
    user_id: i32,
    contest_id: i32,
) -> QueryResult<Vec<(Submission, ContestProblem, User)>> {
    submission::table
        .filter(submission::user_id.eq(user_id))
        .inner_join(contest_problems::table)
        .inner_join(user::table)
        .filter(contest_problems::contest_id.eq(contest_id))
        .order_by(submission::submission_instant.desc())
        .select((SUBMISSION_COLUMNS, CONTEST_PROBLEMS_COLUMNS, USER_COLUMNS))
        .load::<(Submission, ContestProblem, User)>(connection)
}

pub fn get_submissions_user_by_contest_problem(
    connection: &mut PgConnection,
    user_id: i32,
    contest_id: i32,
    problem_label: &str,
) -> QueryResult<Vec<(Submission, ContestProblem, User)>> {
    submission::table
        .filter(submission::user_id.eq(user_id))
        .inner_join(contest_problems::table)
        .inner_join(user::table)
        .filter(contest_problems::contest_id.eq(contest_id))
        .filter(contest_problems::label.eq(problem_label))
        .order_by(submission::submission_instant.desc())
        .select((SUBMISSION_COLUMNS, CONTEST_PROBLEMS_COLUMNS, USER_COLUMNS))
        .load::<(Submission, ContestProblem, User)>(connection)
}
