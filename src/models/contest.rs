use chrono::prelude::*;
use diesel::insert_into;
use diesel::prelude::*;

use crate::schema::{contest, contest_problems};

#[derive(Queryable)]
pub struct Contest {
    pub id: i32,
    pub name: String,
    pub start_instant: Option<NaiveDateTime>,
    pub end_instant: Option<NaiveDateTime>,
    pub creation_user_id: i32,
    pub creation_instant: NaiveDateTime,
    pub grade_ratio: Option<i32>,
    pub grade_after_ratio: Option<i32>,
}

use diesel::sql_types;

#[derive(QueryableByName)]
pub struct ContestWithAcs {
    #[diesel(sql_type = sql_types::Integer)]
    pub id: i32,
    #[diesel(sql_type = sql_types::Text)]
    pub name: String,
    #[diesel(sql_type = sql_types::Nullable<sql_types::Timestamp>)]
    pub start_instant: Option<NaiveDateTime>,
    #[diesel(sql_type = sql_types::Nullable<sql_types::Timestamp>)]
    pub end_instant: Option<NaiveDateTime>,
    #[diesel(sql_type = sql_types::Integer)]
    pub creation_user_id: i32,
    #[diesel(sql_type = sql_types::Timestamp)]
    pub creation_instant: NaiveDateTime,
    #[diesel(sql_type = sql_types::Nullable<sql_types::Integer>)]
    pub grade_ratio: Option<i32>,
    #[diesel(sql_type = sql_types::Nullable<sql_types::Integer>)]
    pub grade_after_ratio: Option<i32>,
    #[diesel(sql_type = sql_types::Integer)]
    pub accepted_count: i32,
    #[diesel(sql_type = sql_types::Integer)]
    pub accepted_after_count: i32,
    #[diesel(sql_type = sql_types::Integer)]
    pub problem_count: i32,
}

#[derive(Insertable)]
#[diesel(table_name = contest)]
pub struct NewContest {
    pub name: String,
    pub start_instant: Option<NaiveDateTime>,
    pub end_instant: Option<NaiveDateTime>,
    pub creation_user_id: i32,
    pub creation_instant: NaiveDateTime,
    pub grade_ratio: Option<i32>,
    pub grade_after_ratio: Option<i32>,
}

pub const CONTEST_COLUMNS: (
    contest::id,
    contest::name,
    contest::start_instant,
    contest::end_instant,
    contest::creation_user_id,
    contest::creation_instant,
    contest::grade_ratio,
    contest::grade_after_ratio,
) = (
    contest::id,
    contest::name,
    contest::start_instant,
    contest::end_instant,
    contest::creation_user_id,
    contest::creation_instant,
    contest::grade_ratio,
    contest::grade_after_ratio,
);

pub fn insert_contest(
    connection: &mut PgConnection,
    new_contest: NewContest,
) -> QueryResult<Contest> {
    insert_into(contest::table)
        .values(new_contest)
        .execute(connection)?;
    contest::table.order(contest::id.desc()).first(connection)
}

pub fn get_contests_with_acs(
    connection: &mut PgConnection,
    user_id: i32,
) -> QueryResult<Vec<ContestWithAcs>> {
    diesel::sql_query(
        r#"
    with first_ac as (
        select
            min(submission_instant) as first_ac_submission_instant,
            contest_problem_id,
            submission.user_id
        from submission
        where submission.verdict = 'AC'
        and user_id = $1
        group by submission.user_id, submission.contest_problem_id
    )
    select
        contest.id,
        contest.name,
        contest.start_instant,
        contest.end_instant,
        contest.creation_user_id,
        contest.creation_instant,
        contest.grade_ratio,
        contest.grade_after_ratio,
        coalesce(cast(count(first_ac_submission_instant) filter (where
            first_ac_submission_instant >= contest.start_instant
            and first_ac_submission_instant <= contest.end_instant
        ) as int), 0) as accepted_count,
        coalesce(cast(count(first_ac_submission_instant) filter (where
            first_ac_submission_instant > contest.end_instant
        ) as int), 0) as accepted_after_count,
        cast(count(contest_problems.id) as int) as problem_count
    from contest
    inner join contest_problems on contest_problems.contest_id = contest.id
    left join first_ac on first_ac.contest_problem_id = contest_problems.id
    group by contest.id
    order by creation_instant desc
    "#,
    )
    .bind::<sql_types::Integer, _>(user_id)
    .load(connection)
}

pub fn get_contests(connection: &mut PgConnection) -> QueryResult<Vec<Contest>> {
    contest::table
        .order(contest::creation_instant.desc())
        .load(connection)
}

pub fn get_contest_by_id(connection: &mut PgConnection, id: i32) -> QueryResult<Contest> {
    contest::table.filter(contest::id.eq(id)).first(connection)
}

pub fn get_contest_by_contest_problem_id(
    connection: &mut PgConnection,
    contest_problem_id: i32,
) -> QueryResult<Contest> {
    contest::table
        .inner_join(contest_problems::table)
        .filter(contest_problems::id.eq(contest_problem_id))
        .select(CONTEST_COLUMNS)
        .first(connection)
}

#[derive(Insertable)]
#[diesel(table_name = contest_problems)]
pub struct NewContestProblems {
    pub label: String,
    pub contest_id: i32,
    pub problem_id: String,
}

pub fn relate_problem(
    connection: &mut PgConnection,
    new_contest_problems: NewContestProblems,
) -> QueryResult<()> {
    insert_into(contest_problems::table)
        .values(new_contest_problems)
        .execute(connection)?;
    Ok(())
}
