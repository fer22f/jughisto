use chrono::prelude::*;
use diesel::insert_into;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use serde::Serialize;

use crate::schema::{contest_problems, problem};

#[derive(Queryable)]
pub struct Problem {
    pub id: String,
    pub name: String,
    pub memory_limit_bytes: i32,
    pub time_limit_ms: i32,
    pub checker_path: String,
    pub checker_language: String,
    pub validator_path: String,
    pub validator_language: String,
    pub main_solution_path: String,
    pub main_solution_language: String,
    pub test_count: i32,
    pub test_pattern: String,
    pub status: String,
    pub creation_user_id: i32,
    pub creation_instant: NaiveDateTime,
}

#[derive(Insertable)]
#[diesel(table_name = problem)]
pub struct NewProblem {
    pub id: String,
    pub name: String,
    pub memory_limit_bytes: i32,
    pub time_limit_ms: i32,
    pub checker_path: String,
    pub checker_language: String,
    pub validator_path: String,
    pub validator_language: String,
    pub main_solution_path: String,
    pub main_solution_language: String,
    pub test_count: i32,
    pub test_pattern: String,
    pub status: String,
    pub creation_user_id: i32,
    pub creation_instant: NaiveDateTime,
}

#[derive(Queryable, Serialize)]
pub struct ProblemByContest {
    pub contest_id: i32,
    pub id: i32,
    pub name: String,
    pub label: String,
}

pub fn get_problems_by_contest_id(
    connection: &mut PgConnection,
    contest_id: i32,
) -> QueryResult<Vec<ProblemByContest>> {
    problem::table
        .inner_join(contest_problems::table)
        .filter(contest_problems::contest_id.eq(contest_id))
        .select((
            contest_problems::contest_id,
            contest_problems::id,
            problem::name,
            contest_problems::label,
        ))
        .order(contest_problems::label)
        .load(connection)
}

use diesel::sql_types;

#[derive(QueryableByName, Debug)]
pub struct ProblemByContestWithScore {
    #[diesel(sql_type = sql_types::Nullable<sql_types::Text>)]
    pub user_name: Option<String>,
    #[diesel(sql_type = sql_types::Nullable<sql_types::Timestamp>)]
    pub first_ac_submission_instant: Option<NaiveDateTime>,
    #[diesel(sql_type = sql_types::Integer)]
    pub failed_submissions: i32,
    #[diesel(sql_type = sql_types::Integer)]
    pub id: i32,
    #[diesel(sql_type = sql_types::Text)]
    pub name: String,
    #[diesel(sql_type = sql_types::Text)]
    pub label: String,
    #[diesel(sql_type = sql_types::Integer)]
    pub memory_limit_bytes: i32,
    #[diesel(sql_type = sql_types::Integer)]
    pub time_limit_ms: i32,
    #[diesel(sql_type = sql_types::Integer)]
    pub user_accepted_count: i32,
}

pub fn get_problems_user_with_score(
    connection: &mut PgConnection,
    user_id: i32,
) -> QueryResult<Vec<ProblemByContestWithScore>> {
    diesel::sql_query(
        r#"
        with first_ac as (
            select
                min(submission_instant) as first_ac_submission_instant,
                contest_problem_id,
                submission.user_id
            from submission
            where submission.verdict = 'AC'
            group by submission.user_id, submission.contest_problem_id
        ), failed_submissions as (
            select
                submission.user_id,
                submission.contest_problem_id,
                cast(count(*) as int) as count
            from submission
            left join first_ac on first_ac.contest_problem_id = submission.contest_problem_id
                and submission.user_id = first_ac.user_id
            where (
                first_ac_submission_instant is null or
                submission.submission_instant < first_ac.first_ac_submission_instant
            )
            group by submission.user_id, submission.contest_problem_id
        ), user_acs_count as (
            select
                cast(count(distinct submission.user_id) as int) as user_accepted_count,
                submission.contest_problem_id
            from submission
            where submission.verdict = 'AC'
            group by submission.contest_problem_id
        )
        select
            "user".name as user_name,
            first_ac_submission_instant,
            coalesce(failed_submissions.count, 0) as failed_submissions,
            contest_problems.id,
            problem.name,
            contest_problems.label,
            problem.memory_limit_bytes,
            problem.time_limit_ms,
            coalesce(user_acs_count.user_accepted_count, 0) as user_accepted_count
        from contest_problems
        inner join problem on problem.id = contest_problems.problem_id
        inner join "user" on "user".id = $1
        left join failed_submissions
            on failed_submissions.contest_problem_id = contest_problems.id
            and failed_submissions.user_id = "user".id
        left join first_ac
            on first_ac.contest_problem_id = contest_problems.id
            and first_ac.user_id = "user".id
        left join user_acs_count
            on user_acs_count.contest_problem_id = contest_problems.id
        order by contest_problems.label
    "#,
    )
    .bind::<sql_types::Integer, _>(user_id)
    .load(connection)
}

pub fn get_problems_user_by_contest_id_with_score(
    connection: &mut PgConnection,
    user_id: i32,
    contest_id: i32,
) -> QueryResult<Vec<ProblemByContestWithScore>> {
    diesel::sql_query(
        r#"
        with first_ac as (
            select
                min(submission_instant) as first_ac_submission_instant,
                contest_problem_id,
                submission.user_id
            from submission
            where submission.verdict = 'AC'
            group by submission.user_id, submission.contest_problem_id
        ), failed_submissions as (
            select
                submission.user_id,
                submission.contest_problem_id,
                cast(count(*) as int) as count
            from submission
            left join first_ac on first_ac.contest_problem_id = submission.contest_problem_id
                and submission.user_id = first_ac.user_id
            where (
                first_ac_submission_instant is null or
                submission.submission_instant < first_ac.first_ac_submission_instant
            )
            group by submission.user_id, submission.contest_problem_id
        ), user_acs_count as (
            select
                cast(count(distinct submission.user_id) as int) as user_accepted_count,
                submission.contest_problem_id
            from submission
            where submission.verdict = 'AC'
            group by submission.contest_problem_id
        )
        select
            "user".name as user_name,
            first_ac_submission_instant,
            coalesce(failed_submissions.count, 0) as failed_submissions,
            contest_problems.id,
            problem.name,
            contest_problems.label,
            problem.memory_limit_bytes,
            problem.time_limit_ms,
            coalesce(user_acs_count.user_accepted_count, 0) as user_accepted_count
        from contest_problems
        inner join problem on problem.id = contest_problems.problem_id
        inner join "user" on "user".id = $1
        left join failed_submissions
            on failed_submissions.contest_problem_id = contest_problems.id
            and failed_submissions.user_id = "user".id
        left join first_ac
            on first_ac.contest_problem_id = contest_problems.id
            and first_ac.user_id = "user".id
        left join user_acs_count
            on user_acs_count.contest_problem_id = contest_problems.id
        where contest_id = $2
        order by contest_problems.label
    "#,
    )
    .bind::<sql_types::Integer, _>(user_id)
    .bind::<sql_types::Integer, _>(contest_id)
    .load(connection)
}

pub fn get_problems_by_contest_id_with_score(
    connection: &mut PgConnection,
    contest_id: i32,
) -> QueryResult<Vec<ProblemByContestWithScore>> {
    diesel::sql_query(
        r#"
        with first_ac as (
            select
                min(submission_instant) as first_ac_submission_instant,
                contest_problem_id,
                submission.user_id
            from submission
            where submission.verdict = 'AC'
            group by submission.user_id, submission.contest_problem_id
        ), failed_submissions as (
            select
                submission.user_id,
                submission.contest_problem_id,
                cast(count(*) as int) as count
            from submission
            left join first_ac on first_ac.contest_problem_id = submission.contest_problem_id
                and submission.user_id = first_ac.user_id
            where (
                first_ac_submission_instant is null or
                submission.submission_instant < first_ac.first_ac_submission_instant
            )
            group by submission.user_id, submission.contest_problem_id
        ), all_submitters as (
            select user_id, contest_id
            from submission
            inner join contest_problems on contest_problems.id = submission.contest_problem_id
            group by user_id, contest_id
        ), user_acs_count as (
            select
                cast(count(distinct submission.user_id) as int) as user_accepted_count,
                submission.contest_problem_id
            from submission
            where submission.verdict = 'AC'
            group by submission.contest_problem_id
        )
        (select
            null as user_name,
            null as first_ac_submission_instant,
            0 as failed_submissions,
            contest_problems.id,
            problem.name,
            contest_problems.label,
            problem.memory_limit_bytes,
            problem.time_limit_ms,
            0 as user_accepted_count
        from contest_problems
        inner join problem on problem.id = contest_problems.problem_id
        where contest_problems.contest_id = $1
        order by contest_problems.label)
        union all
        (select
            "user".name as user_name,
            first_ac_submission_instant,
            coalesce(failed_submissions.count, 0) as failed_submissions,
            contest_problems.id,
            problem.name,
            contest_problems.label,
            problem.memory_limit_bytes,
            problem.time_limit_ms,
            coalesce(user_acs_count.user_accepted_count, 0) as user_accepted_count
        from contest_problems
        inner join problem on problem.id = contest_problems.problem_id
        inner join all_submitters on all_submitters.contest_id = $1
        inner join "user" on "user".id = all_submitters.user_id
        left join failed_submissions
            on failed_submissions.contest_problem_id = contest_problems.id
            and failed_submissions.user_id = "user".id
        left join first_ac
            on first_ac.contest_problem_id = contest_problems.id
            and first_ac.user_id = "user".id
        left join user_acs_count
            on user_acs_count.contest_problem_id = contest_problems.id
        where contest_problems.contest_id = $1
        order by "user".name, contest_problems.label)
    "#,
    )
    .bind::<sql_types::Integer, _>(contest_id)
    .load(connection)
}

pub fn get_problem_by_contest_id_label(
    connection: &mut PgConnection,
    contest_id: i32,
    problem_label: &str,
) -> QueryResult<ProblemByContest> {
    problem::table
        .inner_join(contest_problems::table)
        .filter(contest_problems::contest_id.eq(contest_id))
        .filter(contest_problems::label.eq(problem_label))
        .select((
            contest_problems::contest_id,
            contest_problems::id,
            problem::name,
            contest_problems::label,
        ))
        .order(contest_problems::label)
        .first(connection)
}

#[derive(Queryable)]
pub struct ProblemByContestMetadata {
    pub contest_id: i32,
    pub id: String,
    pub memory_limit_bytes: i32,
    pub time_limit_ms: i32,
    pub checker_path: String,
    pub checker_language: String,
    pub validator_path: String,
    pub validator_language: String,
    pub main_solution_path: String,
    pub main_solution_language: String,
    pub test_count: i32,
    pub test_pattern: String,
    pub status: String,
}

pub fn get_problem_by_contest_id_metadata(
    connection: &mut PgConnection,
    contest_problem_id: i32,
) -> QueryResult<ProblemByContestMetadata> {
    problem::table
        .inner_join(contest_problems::table)
        .filter(contest_problems::id.eq(contest_problem_id))
        .select((
            contest_problems::contest_id,
            problem::id,
            problem::memory_limit_bytes,
            problem::time_limit_ms,
            problem::checker_path,
            problem::checker_language,
            problem::validator_path,
            problem::validator_language,
            problem::main_solution_path,
            problem::main_solution_language,
            problem::test_count,
            problem::test_pattern,
            problem::status,
        ))
        .first(connection)
}

pub fn upsert_problem(
    connection: &mut PgConnection,
    new_problem: NewProblem,
) -> QueryResult<Problem> {
    match problem::table
        .filter(problem::id.eq(&new_problem.id))
        .first::<Problem>(connection)
        .optional()?
    {
        Some(p) => Ok(p),
        None => {
            insert_into(problem::table)
                .values(&new_problem)
                .execute(connection)?;
            problem::table
                .filter(problem::id.eq(&new_problem.id))
                .first::<Problem>(connection)
        }
    }
}
