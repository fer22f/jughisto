use std::io::{Read, Seek};

use lazy_static::lazy_static;
use regex::{Captures, Regex};
use zip::ZipArchive;

mod error {
    use std::io;

    use quick_xml::de::DeError;
    use thiserror::Error;
    use zip::result::ZipError;

    #[derive(Error, Debug)]
    pub enum ImportContestError {
        #[error(transparent)]
        Zip(#[from] ZipError),
        #[error(transparent)]
        XmlDecode(#[from] DeError),
        #[error(transparent)]
        Io(#[from] io::Error),
    }
}

pub use error::ImportContestError;

mod xml {
    use prelude::*;
    use quick_xml::de::from_str;

    pub fn get_from_zip<T: for<'de> Deserialize<'de>, R: Read + Seek>(
        zip: &mut ZipArchive<R>,
        path: &str,
    ) -> Result<T, ImportContestError> {
        let xml = super::read_string_from_zip_by_name(&mut *zip, path)?;
        let deserialized: T = from_str(&xml)?;
        Ok(deserialized)
    }

    pub mod prelude {
        pub use std::io::{Read, Seek};

        pub use serde::Deserialize;
        pub use zip::ZipArchive;

        pub use super::super::ImportContestError;
    }

    pub mod contest {
        use super::prelude::*;

        #[derive(Deserialize, Debug)]
        pub struct Contest {
            #[serde(rename = "@url")]
            pub url: String,
            pub names: Names,
            pub problems: Problems,
        }

        #[derive(Deserialize, Debug)]
        pub struct Names {
            pub name: Vec<Name>,
        }

        #[derive(Deserialize, Debug)]
        pub struct Name {
            #[serde(rename = "@language")]
            pub language: String,
            #[serde(rename = "@value")]
            pub value: String,
        }

        #[derive(Deserialize, Debug)]
        pub struct Problems {
            pub problem: Vec<Problem>,
        }

        #[derive(Deserialize, Debug)]
        pub struct Problem {
            #[serde(rename = "@index")]
            pub index: String,
            #[serde(rename = "@url")]
            pub url: String,
        }

        pub fn get_from_zip<R: Read + Seek>(
            zip: &mut ZipArchive<R>,
        ) -> Result<Contest, ImportContestError> {
            super::get_from_zip::<Contest, R>(&mut *zip, "contest.xml")
        }
    }

    pub mod problem {
        use super::prelude::*;

        #[derive(Deserialize, Debug)]
        pub struct Problem {
            #[serde(rename = "@url")]
            pub url: String,
            #[serde(rename = "@revision")]
            pub revision: String,
            #[serde(rename = "@short-name")]
            pub short_name: String,
            pub names: Names,
            pub statements: Statements,
            pub judging: Judging,
            pub files: Files,
            pub assets: Assets,
            pub properties: Properties,
            pub stresses: Stresses,
        }

        #[derive(Deserialize, Debug)]
        pub struct Names {
            pub name: Vec<Name>,
        }

        #[derive(Deserialize, Debug)]
        pub struct Name {
            #[serde(rename = "@language")]
            pub language: String,
            #[serde(rename = "@value")]
            pub value: String,
        }

        #[derive(Deserialize, Debug)]
        pub struct Statements {
            pub statement: Vec<Statement>,
        }

        #[derive(Deserialize, Debug)]
        pub struct Statement {
            #[serde(rename = "@charset")]
            pub charset: Option<String>,
            #[serde(rename = "@language")]
            pub language: String,
            #[serde(rename = "@mathjax")]
            pub mathjax: Option<bool>,
            #[serde(rename = "@path")]
            pub path: String,
            #[serde(rename = "@type")]
            pub r#type: String,
        }

        #[derive(Deserialize, Debug)]
        pub struct Judging {
            #[serde(rename = "@cpu-name")]
            pub cpu_name: String,
            #[serde(rename = "@cpu-speed")]
            pub cpu_speed: String,
            #[serde(rename = "@input-file")]
            pub input_file: String,
            #[serde(rename = "@output-file")]
            pub output_file: String,
            pub testset: Vec<Testset>,
        }

        #[derive(Deserialize, Debug)]
        pub struct Testset {
            #[serde(rename = "@name")]
            pub name: String,
            #[serde(rename = "time-limit")]
            pub time_limit: String,
            #[serde(rename = "memory-limit")]
            pub memory_limit: String,
            #[serde(rename = "test-count")]
            pub test_count: String,
            #[serde(rename = "input-path-pattern")]
            pub input_path_pattern: String,
            #[serde(rename = "answer-path-pattern")]
            pub answer_path_pattern: String,
            pub tests: Tests,
        }

        #[derive(Deserialize, Debug)]
        pub struct Tests {
            pub test: Vec<Test>,
        }

        #[derive(Deserialize, Debug)]
        pub struct Test {
            #[serde(rename = "@method")]
            pub method: Option<String>,
            #[serde(rename = "@sample")]
            pub sample: Option<bool>,
            #[serde(rename = "@description")]
            pub description: Option<String>,
            #[serde(rename = "@cmd")]
            pub cmd: Option<String>,
        }

        #[derive(Deserialize, Debug)]
        pub struct Files {
            pub resources: Resources,
            pub executables: Executables,
        }

        #[derive(Deserialize, Debug)]
        pub struct Resources {
            pub file: Vec<File>,
        }

        #[derive(Deserialize, Debug)]
        pub struct File {
            #[serde(rename = "@path")]
            pub path: String,
        }

        #[derive(Deserialize, Debug)]
        pub struct Executables {
            pub executable: Vec<Executable>,
        }

        #[derive(Deserialize, Debug)]
        pub struct Executable {
            pub source: Source,
            pub binary: Binary,
        }

        #[derive(Deserialize, Debug)]
        pub struct Source {
            #[serde(rename = "@path")]
            pub path: String,
            #[serde(rename = "@type")]
            pub r#type: String,
        }

        #[derive(Deserialize, Debug)]
        pub struct Binary {
            #[serde(rename = "@path")]
            pub path: String,
            #[serde(rename = "@type")]
            pub r#type: String,
        }

        #[derive(Deserialize, Debug)]
        pub struct Assets {
            pub checker: Checker,
            pub validators: Validators,
            pub solutions: Solutions,
        }

        #[derive(Deserialize, Debug)]
        pub struct Checker {
            #[serde(rename = "@type")]
            pub r#type: String,
            pub source: Source,
            pub binary: Binary,
            pub copy: Copy,
            pub testset: CheckerTestset,
        }

        #[derive(Deserialize, Debug)]
        pub struct CheckerTestset {
            #[serde(rename = "test-count")]
            pub test_count: String,
            #[serde(rename = "input-path-pattern")]
            pub input_path_pattern: String,
            #[serde(rename = "answer-path-pattern")]
            pub answer_path_pattern: String,
            pub tests: VerdictTests,
        }

        #[derive(Deserialize, Debug)]
        pub struct Copy {
            #[serde(rename = "@path")]
            pub path: String,
        }

        #[derive(Deserialize, Debug)]
        pub struct Validators {
            pub validator: Vec<Validator>,
        }

        #[derive(Deserialize, Debug)]
        pub struct Validator {
            pub source: Source,
            pub binary: Binary,
            pub testset: ValidatorTestset,
        }

        #[derive(Deserialize, Debug)]
        pub struct ValidatorTestset {
            #[serde(rename = "test-count")]
            pub test_count: String,
            #[serde(rename = "input-path-pattern")]
            pub input_path_pattern: String,
            pub tests: VerdictTests,
        }

        #[derive(Deserialize, Debug)]
        pub struct VerdictTests {
            pub test: Option<Vec<VerdictTest>>,
        }

        #[derive(Deserialize, Debug)]
        pub struct VerdictTest {
            #[serde(rename = "@verdict")]
            pub verdict: String,
        }

        #[derive(Deserialize, Debug)]
        pub struct Solutions {
            pub solution: Vec<Solution>,
        }

        #[derive(Deserialize, Debug)]
        pub struct Solution {
            #[serde(rename = "@tag")]
            pub tag: String,
            pub source: Source,
            pub binary: Binary,
        }

        #[derive(Deserialize, Debug)]
        pub struct Properties {
            pub property: Option<Vec<Property>>,
        }

        #[derive(Deserialize, Debug)]
        pub struct Property {
            #[serde(rename = "@name")]
            pub name: String,
            #[serde(rename = "@value")]
            pub value: String,
        }

        #[derive(Deserialize, Debug)]
        pub struct Stresses {
            #[serde(rename = "stress-count")]
            pub stress_count: String,
            #[serde(rename = "stress-path-pattern")]
            pub stress_path_pattern: String,
            pub list: StressList,
        }

        #[derive(Deserialize, Debug)]
        pub struct StressList {}

        #[derive(Deserialize, Debug)]
        pub struct Tags {
            pub tag: Option<Vec<Tag>>,
        }

        #[derive(Deserialize, Debug)]
        pub struct Tag {
            #[serde(rename = "@value")]
            pub value: String,
        }

        pub fn get_from_zip<R: Read + Seek>(
            zip: &mut ZipArchive<R>,
            name: &str,
        ) -> Result<Problem, ImportContestError> {
            super::get_from_zip::<Problem, R>(&mut *zip, name)
        }
    }
}

fn read_string_from_zip_by_name<R: Read + Seek>(
    zip: &mut ZipArchive<R>,
    name: &str,
) -> Result<String, ImportContestError> {
    let mut content = String::new();
    zip.by_name(name)?.read_to_string(&mut content)?;
    Ok(content)
}

pub use xml::contest::Contest;
pub use xml::problem::Problem;

pub type ContestProblemsZipReport<R> = (Contest, Vec<(String, Problem)>, ZipArchive<R>, String);

pub fn import_file<R: Read + Seek>(
    reader: R,
) -> Result<ContestProblemsZipReport<R>, ImportContestError> {
    let mut report = String::new();

    let mut zip = ZipArchive::new(reader)?;
    let contest = xml::contest::get_from_zip(&mut zip)?;

    report.push_str(&format!("{:#?}", contest));
    report.push_str("\n---\n");

    let mut problems: Vec<(String, Problem)> = Vec::new();

    lazy_static! {
        static ref PROBLEM_XML_PATH_REGEX: Regex =
            Regex::new(r"^(problems/.*)/problem.xml$").unwrap();
    }

    for name in zip
        .file_names()
        .filter(|name| PROBLEM_XML_PATH_REGEX.is_match(name))
        .map(std::convert::Into::into)
        .collect::<Vec<String>>()
    {
        println!("{}", name);
        report.push_str(&name);
        let problem = xml::problem::get_from_zip(&mut zip, &name)?;
        report.push_str("\n---\n");
        report.push_str(&format!("{:#?}", problem));
        report.push_str("\n---\n");
        report.push_str(&read_string_from_zip_by_name(&mut zip, &name)?);
        report.push_str("---\n");
        report.push('\n');

        problems.push((
            PROBLEM_XML_PATH_REGEX
                .captures(&name)
                .unwrap()
                .get(1)
                .unwrap()
                .as_str()
                .into(),
            problem,
        ));
    }

    Ok((contest, problems, zip, report))
}

#[must_use] pub fn format_width(pattern_path: &str, i: usize) -> String {
    lazy_static! {
        static ref WIDTH_REGEX: Regex = Regex::new(r"%0(\d)+d").unwrap();
    }
    WIDTH_REGEX
        .replace(pattern_path, |caps: &Captures| {
            format!("{:0width$}", i, width = caps[1].parse().unwrap())
        })
        .into()
}
