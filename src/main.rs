use std::env;
use std::error::Error;
use std::sync::Arc;
use std::time::Duration;

use base64::prelude::*;
use actix_files::Files;
use actix_identity::IdentityMiddleware;
use actix_session::storage::CookieSessionStore;
use actix_session::SessionMiddleware;
use actix_web::cookie::Key;
use actix_web::middleware::ErrorHandlers;
use actix_web::web::Data;
use actix_web::{http, middleware, web, App, HttpServer};
use actix_web_flash_messages::storage::CookieMessageStore;
use actix_web_flash_messages::{FlashMessagesFramework, Level};
use broadcaster::Broadcaster;
use chrono_tz::Tz;
use dashmap::DashMap;
use diesel::pg::PgConnection;
use diesel::r2d2::ConnectionManager;
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use futures::TryFutureExt;
use handlebars::Handlebars;
use listenfd::ListenFd;
use models::{contest, problem, submission, user};
use problem::ProblemByContestMetadata;
use queue::job_protocol::job_queue_server::JobQueueServer;
use queue::job_protocol::{job, job_result, Job, JobResult, Language};
use queue::JobQueuer;
use submission::{Submission, SubmissionCompletion};
use tokio::sync::broadcast;
use tonic::transport::Server;

mod broadcaster;
mod import_contest;
mod language;
mod models;
mod pages;
mod queue;
mod schema;
mod setup;

async fn update_database(
    mut job_result_receiver: broadcast::Receiver<JobResult>,
    pool: pages::prelude::DbPool,
) -> Result<(), pages::prelude::PageError> {
    loop {
        let job_result = job_result_receiver.recv().await.unwrap();
        if let JobResult {
            which: Some(job_result::Which::Judgement(judgement)),
            ..
        } = job_result
        {
            let mut connection = pool.get().unwrap();
            submission::complete_submission(
                &mut connection,
                SubmissionCompletion {
                    uuid: job_result.uuid,
                    verdict: match job_result::judgement::Verdict::try_from(judgement.verdict) {
                        Ok(job_result::judgement::Verdict::Accepted) => "AC".into(),
                        Ok(job_result::judgement::Verdict::WrongAnswer) => "WA".into(),
                        Ok(job_result::judgement::Verdict::CompilationError) => "CE".into(),
                        Ok(job_result::judgement::Verdict::TimeLimitExceeded) => "TL".into(),
                        Ok(job_result::judgement::Verdict::MemoryLimitExceeded) => "ML".into(),
                        Ok(job_result::judgement::Verdict::RuntimeError) => "RE".into(),
                        _ => "XX".into(),
                    },
                    judge_start_instant: chrono::NaiveDateTime::parse_from_str(
                        &judgement.judge_start_instant,
                        "%Y-%m-%dT%H:%M:%S%.f",
                    )
                    .unwrap(),
                    judge_end_instant: chrono::NaiveDateTime::parse_from_str(
                        &judgement.judge_end_instant,
                        "%Y-%m-%dT%H:%M:%S%.f",
                    )
                    .unwrap(),
                    memory_kib: Some(judgement.memory_kib),
                    time_ms: Some(judgement.time_ms),
                    time_wall_ms: Some(judgement.time_wall_ms),
                    error_output: Some(judgement.error_output),
                    failed_test: match judgement.failed_test {
                        0 => None,
                        test => Some(test),
                    },
                },
            )
            .unwrap();
        }
    }
}

fn create_job_from_submission(submission: Submission, metadata: ProblemByContestMetadata) -> Job {
    Job {
        uuid: submission.uuid,
        language: submission.language,
        time_limit_ms: metadata.time_limit_ms,
        memory_limit_kib: metadata.memory_limit_bytes / 1_024,

        which: Some(job::Which::Judgement(job::Judgement {
            source_text: submission.source_text,
            test_count: metadata.test_count,
            test_pattern: format!("./{}/{}", metadata.id, metadata.test_pattern),
            checker_language: metadata.checker_language,
            checker_source_path: format!("./{}/{}", metadata.id, metadata.checker_path),
        })),
    }
}

pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

#[actix_web::main]
async fn main() -> Result<(), Box<dyn Error>> {
    std::env::set_var("RUST_LOG", "info");
    env_logger::init();

    let private_key = BASE64_STANDARD.decode(
        env::var("IDENTITY_SECRET_KEY")
            .expect("IDENTITY_SECRET_KEY environment variable is not set"),
    )
    .unwrap();

    let database_url =
        env::var("DATABASE_URL").expect("DATABASE_URL environment variable is not set");
    let manager = ConnectionManager::<PgConnection>::new(database_url);
    let pool = r2d2::Pool::builder()
        .max_size(50)
        .connection_timeout(Duration::from_secs(1))
        .build(manager)
        .expect("Failed to create pool.");

    pool.get()
        .expect("couldn't get connection from pool")
        .run_pending_migrations(MIGRATIONS)
        .unwrap();
    setup::setup_admin(&mut pool.get().expect("couldn't get connection from the pool"));

    let mut handlebars = Handlebars::new();
    handlebars
        .register_templates_directory(".hbs", "./templates")
        .expect("Couldn't find templates directory");
    let handlebars_ref = web::Data::new(handlebars);

    let languages = Arc::new(DashMap::<String, Language>::new());

    let tz: Tz = env::var("TZ")
        .expect("TZ environment variable is not set")
        .parse()
        .expect("Invalid timezone in environment variable TZ");

    let (job_sender, job_receiver) = async_channel::unbounded();
    let (job_result_sender, job_result_receiver) = broadcast::channel(40);

    for (submission, metadata) in submission::get_waiting_judge_submissions(
        &mut pool.get().expect("couldn't get connection from pool"),
    )? {
        job_sender
            .send(create_job_from_submission(submission, metadata))
            .await?;
    }

    let broadcaster = Broadcaster::create(job_result_receiver);

    let mut listenfd = ListenFd::from_env();
    let job_sender_data = job_sender.clone();
    let job_result_sender_data = job_result_sender.clone();
    let languages_data = languages.clone();
    let pool_data = pool.clone();
    let mut server = HttpServer::new(move || {
        App::new()
            .app_data(Data::new(pool_data.clone()))
            .app_data(Data::new(job_sender_data.clone()))
            .app_data(Data::new(job_result_sender_data.clone()))
            .app_data(Data::new(languages_data.clone()))
            .app_data(Data::new(tz))
            .wrap(ErrorHandlers::new().handler(http::StatusCode::UNAUTHORIZED, pages::render_401))
            .wrap(ErrorHandlers::new().handler(http::StatusCode::BAD_REQUEST, pages::render_400))
            .wrap(
                FlashMessagesFramework::builder(
                    CookieMessageStore::builder(Key::from(&private_key)).build(),
                )
                .minimum_level(Level::Info)
                .build(),
            )
            .wrap(IdentityMiddleware::default())
            .wrap(
                SessionMiddleware::builder(CookieSessionStore::default(), Key::from(&private_key))
                    .cookie_name("user".into())
                    .cookie_secure(false)
                    .build(),
            )
            .wrap(middleware::Logger::default())
            .app_data(broadcaster.clone())
            .app_data(handlebars_ref.clone())
            .service(
                web::scope("/jughisto")
                    .service(pages::get_login::get_login)
                    .service(pages::get_me::get_me)
                    .service(pages::change_password::change_password)
                    .service(pages::post_login::post_login)
                    .service(pages::post_logout::post_logout)
                    .service(pages::get_main::get_main)
                    .service(pages::get_contests::get_contests)
                    .service(pages::get_contest_by_id::get_contest_by_id)
                    .service(pages::get_contest_scoreboard_by_id::get_contest_scoreboard_by_id)
                    .service(pages::get_contest_problem_by_id_label::get_contest_problem_by_id_label)
                    .service(pages::get_submissions_me::get_submissions_me)
                    .service(pages::get_submission::get_submission)
                    .service(pages::get_submissions::get_submissions)
                    .service(pages::get_submissions_me_by_contest_id::get_submissions_me_by_contest_id)
                    .service(pages::get_submissions_me_by_contest_id_problem_label::get_submissions_me_by_contest_id_problem_label)
                    .service(pages::rejudge_submission::rejudge_submission)
                    .service(pages::create_submission::create_submission)
                    .service(pages::create_contest::create_contest)
                    .service(pages::get_problems::get_problems)
                    .service(pages::get_about::get_about)
                    .service(pages::get_editor::get_editor)
                    .service(pages::create_user::create_user)
                    .service(pages::impersonate_user::impersonate_user)
                    .service(pages::submission_updates::submission_updates)
                    .service(pages::get_problem_by_id_assets::get_problem_by_id_assets)
                    .service(Files::new("/static/", "./static/")),
            )
    });

    server = if let Some(l) = listenfd
        .take_tcp_listener(0)
        .expect("Can't take TCP listener from listenfd")
    {
        server.listen(l)?
    } else {
        server.bind("0.0.0.0:8000")?
    };

    let addr = "0.0.0.0:50051".parse().unwrap();

    let update_dabase_sender = job_result_sender.clone();
    log::info!("Starting at {}", addr);
    tokio::try_join!(
        server.run().map_err(Into::<Box<dyn Error>>::into),
        Server::builder()
            .add_service(JobQueueServer::new(JobQueuer {
                job_receiver,
                job_result_sender,
                languages,
            }))
            .serve(addr)
            .map_err(Into::<Box<dyn Error>>::into),
        update_database(update_dabase_sender.subscribe(), pool.clone()).map_err(Into::<
            Box<dyn Error>,
        >::into)
    )?;

    Ok(())
}
