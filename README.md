Jughisto
===

Work in progress competitive programming judge that is compatible with Polygon packages.

## How to Run

* Make sure the `isolate/` submodule has been fetched
* Copy `isolate/systemd/*` files to `/etc/systemd/system/` and start isolate's
daemon that provides the base cgroups v2 root directory.
* Use docker compose (`docker compose up`), there is a development environment
and a production environment.

## Features

* Polygon contest package support
* Web backend made in Rust with Rocket+Diesel
* Isolation made using [isolate](https://github.com/ioi/isolate)
* Lightweight server-side rendered frontend with SSE updates
* Docker support
* Parallel judging process

## TODO

* Internationalization

## Planned

* Problem creation platform
* Support for high performance competitions (measure speedup)
* Support for energy efficient competitions (measure energy efficiency)
* Support for multiple file programs
